@shift
@echo off
title Allure Online GameServer
color 0A
@ ECHO.
@ ECHO.
@ ECHO.
ECHO --------------------------------------------------------------------------------
ECHO                                  Allure Online
@ ECHO.
ECHO --------------------------------------------------------------------------------
@ ECHO.
@ ECHO                                                          (c) mZer0ne 10.07.2014
@ ECHO.
pause
java -Xmx1024M -XX:+ForceTimeHighResolution -cp GameServer.zip;lib/mysqlserver.jar. app.game.gs.GameServer >> logs.log
pause