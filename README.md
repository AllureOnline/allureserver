# Команды GM: #

Команда       | Описание      | Пример
------------- | ------------- | -------------
addExp|Получить опыт|/addExp количество  
addMoney|Выдать деньги|/addMoney количество
addGoods|Выдать вещь|/addGoods ID количество
addPlayerTreasure|Выдать СВ|/addPlayerTreasure логин количество
gotoPlayer|Телепорт к игроку|/gotoPlayer логин
move|Телепорт по карте|/move x y
kickPlayer|Выкинуть из игры|/kickPlayer логин
forbidPlayerChat|Отключить чат|/forbidPlayerChat логин
forbidPlayerFight|Запретить бои|/forbidPlayerFight логин
forbidPlayerMove|Замарозить на месте|/forbidPlayerMove логин       
setPlayerStamina|Изменение жизни|/setPlayerStamina логин 
pullPlayer|Телепорт к себе|/pullPlayer логин        
addPlayerMoney|Выдать деньги игроку|/addPlayerMoney логин количество        
issueNotice|Объявление|/issueNotice объявление количество
issueAlert|Предупреждения|/issueAlert предупреждения количество                
issueInformation|Информация|/issueInformation информация количество
setExpTimes|Процент опыта|/setExpTimes 10 количество(по умолчанию 1)
setMoneyTimes|Процент вып. денег|/setMoneyTimes 200 количество(по умолчанию 1)
setDropTimes|Процент вып. дропа|/setDropTimes 10 количество(по умолчанию 1)
getScenePlayers|Список игроков на карте|/getScenePlayers карта
getWorldPlayers|Список игроков на сервере|/getWorldPlayers
gotoScene|Телепорт в карту|/gotoScene карта
getSceneList|Список карт|/getSceneList
clearEffects|Убрать эфекты|/clearEffects логин
clearGoods|Удалить вещи|/clearGoods ID вещи
changeGMRight|Изменить полномочия ГМ|/changeGMRight Логин 0-255
getPayedTreasure|总充值元宝数|
getConsumedTreasure|总消费元宝数|
getServerInfo|服务器信息|
reducdPlayerMoney|扣除玩家金钱|
clearPlayerDepot|清空玩家仓库|
clearPlayerEquipments|清空玩家装备|
queryPlayerUserPassword|查询玩家密码|
addPlayerExp|增加玩家经验|
kickUesr|踢除玩家|
addPlayerTreasure|增加玩家元宝|
reducePlayerTreasure|扣除玩家元宝|
createMonster|创建怪物|
addShopGoods|添加商店物品|
createDuplicatte|创建副本|
killMonster|杀死怪物|
deleteMonster|删除怪物|
pullMonster|拉怪物|
getConfig||
setConfig||
surelyPullPlayer||
removeShopGoods||
setShopSaleScale||
setShopPurchaseScale||
setGoodsTreasurePrice||
setGoodsMoneyPrice||
requestQuit||
createGoods||
showNpc||
changeGuildName||
deleteGuild||
sortGuildList||
resetHeroProperties||
addRebateGoods||
removeRebateGoods||
addTask||
removeTask||
changeTaskData||
getUserInfo||
putSecretGoods||