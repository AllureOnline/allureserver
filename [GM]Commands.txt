在服务器窗口输入命令saveAll 保存数据 exit关闭


新版服务端GM命令列表                 [有的命令不能使用，请等待开发。我回进快的完善，

新开服添加首个GM的方法:
注册帐号并登陆游戏创建角色名称后，再服务端控制台中输入gmGrade 老杨 255
即成功升级该角色为GM(注意输入的命令的大小写)

命令                        描述                                例子

addExp                        获得经验                        /addExp 数量  
addMoney                获得金钱                        /addMoney 数量
addGoods                获得商品                        /addGoods 商品id 数量
addPlayerTreasure       获得元宝                        /addPlayerTreasure 玩家角色名称 数量
gotoPlayer                前往到玩家身边                        /gotoPlayer 玩家角色名称
move                        移动到坐标                        /move 坐标x 坐标y
kickPlayer                踢除玩家                        /kickPlayer 玩家角色名称
forbidPlayerChat        禁止玩家发言                        /forbidPlayerChat 玩家角色名称(慎用)
forbidPlayerFight        禁止玩家战斗                        /forbidPlayerFight 玩家角色名称(慎用)
forbidPlayerMove        禁止玩家移动                        /forbidPlayerMove 玩家角色名称(慎用)        
setPlayerStamina        设置玩家活力值                        /setPlayerStamina 玩家角色名称 数值(大于玩家的最大的法力值即提升满)
pullPlayer                移动玩家                        /pullPlayer 玩家角色名称                
putSecretGoods
issueNotice                公告                                /issueNotice 公告内容 数值
issueAlert                警告                                /issueAlert 警告内容 数值                
issueInformation        信息                                /issueInformation 信息内容 数值
setExpTimes                设置经验倍数                        /setExpTimes 10 数值(默认为1倍)
setMoneyTimes                设置金钱倍数                        /setMoneyTimes 200 数值(默认为1倍)
setDropTimes                设置物品暴率                        /setDropTimes 10 数值(默认为1倍)
getScenePlayers                查询本地图玩家列表                /getScenePlayers 地图名
getWorldPlayers                查询世界地图玩家列表                
gotoScene                传送到场景
getSceneList                获取场景列表
clearEffects                清除特效
clearGoods                清除商品
changeGMRight                更改GM权限
getPayedTreasure        总充值元宝数
getConsumedTreasure        总消费元宝数
getServerInfo                服务器信息
reducdPlayerMoney        扣除玩家金钱
clearPlayerDepot        清空玩家仓库
clearPlayerEquipments        清空玩家装备
queryPlayerUserPassword  查询玩家密码
addPlayerExp                增加玩家经验
kickUesr                踢除玩家
addPlayerTreasure        增加玩家元宝
reducePlayerTreasure        扣除玩家元宝
createMonster                创建怪物
surelyPullPlayer        
createDuplicatte        创建副本
killMonster            杀死怪物
deleteMonster            删除怪物
pullMonster            拉怪物
getConfig
setConfig
addShopGoods           添加商店物品
removeShopGoods
setShopSaleScale
setShopPurchaseScale
setGoodsTreasurePrice
setGoodsMoneyPrice
requestQuit
createGoods
showNpc
changeGuildName
deleteGuild
sortGuildList
resetHeroProperties
addRebateGoods
removeRebateGoods
addTask
removeTask
changeTaskData
getUserInfo
addPlayerMoney         给某人屋加钱               /addPlayerMoney 玩家角色名称 数量
