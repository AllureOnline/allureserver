编号	入口名字	最低等级	最高等级	GM等级要求	门派要求	完成过任务	接受了任务	最低任务数据	最高任务数据	物品需求编号	最小物品数量	最大物品数量	要列出的任务组编号	是否会给任务	话语的显示文字	文字颜色(16进制)	选项文字1	选项命令1	选项文字2	选项命令2	选项文字3	选项命令3	选项文字4	选项命令4	选项文字5	选项命令5	选项文字6	选项命令6
id	name	minLevel	maxLevel	gmGrade	race	taskDone	taskDoing	minTaskData	maxTaskData	requiredGoodsID	minGoodsCount	maxGoodsCount	listTaskGroup	offerTask	text	color	options	commands	options	commands	options	commands	options	commands	options	commands	options	commands
1		0	100	0	-1	1510000487	0	0	0	0	0	0	0	0	阁主对我们恩重如山，整个星城阁就是一个大家庭，不会有孤单的感觉。	ffffff	难得啊。											
2	魔凶又现闲聊	0	100	0	-1	0	1510000487	0	1000	0	0	0	0	0	加油加油。	ffffff	呵呵。											
3	魔凶又现1	0	100	0	-1	1510000486	0	0	0	0	0	0	0	1	石斧妖人众多，你只消灭了部分，现在那魔界四凶中的老二魔帝现身，我觉得你一定要快点消灭他。	ffffff	星城阁、倾月轩真是名不虚传。	goto:魔凶又现2										
4	魔凶又现2	0	0	0	-1	0	0	0	0	0	0	0	0	1	啊？呵呵过奖了。好了不说了你消灭了魔帝就去找倾月轩弟子灵月吧。（让聚仙镇的密道守卫青火送你去人魔道，魔帝就在第23层。）	ffffff	好的，我清楚了。	giveTask:1510000487										
5	先头部队	0	100	0	-1	0	1510000486	45	1000	0	0	0	0	0	你来了？你好。我叫幻星。	ffffff	完成任务	completeTask:1510000486										
6	开始	0	100	0	-1	0	0	0	0	0	0	0	0	0	我们的名字都是阁主帮我们取的，至于本名，那么多年不用都快要忘了……	ffffff	现在的挺好听啊，很有意境。											
