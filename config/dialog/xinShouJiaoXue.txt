编号	入口名字	最低等级	最高等级	GM等级要求	门派要求	完成过任务	接受了任务	最低任务数据	最高任务数据	物品需求编号	是否可能赋予任务	话语的显示文字	文字颜色(16进制)	选项文字1	选项命令1	选项文字2	选项命令2	选项文字3	选项命令3	选项文字4	选项命令4	选项文字5	选项命令5	选项文字6	选项命令6
id	name	minLevel	maxLevel	gmGrade	race	taskDone	taskDoing	minTaskData	maxTaskData	requiredGoodsID	offerTask	text	color	options	commands	options	commands	options	commands	options	commands	options	commands	options	commands
2	杀死怪	0	100	0	-1	0	1501000005	1	1	0	0	看来你已经完全掌握了我教给你的技能，孺子可教。	ffffff	继续	completeTask:1501000005;goto:杀死怪后送药										
1	试炼之地	0	100	0	-1	1501000005	0	0	0	0	0	试练之地是蓬莱，青城，罗浮三派祖师施展大法力联手开辟的空间，此空间介于虚实之间，在内死亡可保神魂不灭转世重生，专为增强新入门之弟子道法之用，只有通过了试练才算正式列入我罗浮门墙，闭上双眼，我送你去试炼之地。	ffffff	试炼之地列表	openCopyList:1										
3	没有杀掉怪	0	100	0	-1	0	1501000005	0	0	0	0	那妖魔就在房屋的角落，让我看看你技能学的如何？	ffffff	活用学到的技能，打败怪物											
4	杀怪	0	100	0	-1	0	1501000004	1	1	0	1	学以致用，房屋角落的妖魔被我道法所困，你可尝试用新学习的技能与妖魔战斗。。	ffffff	活用学到的技能，打败怪物	completeTask:1501000004;giveTask:1501000005										
5	没学技能	0	100	0	-1	0	1501000004	0	0	0	1	学会技能之后再来找我吧。	ffffff	打开背包，右键点击技能书，然后将技能拖到到快捷栏											
6	已穿上装备	0	100	0	-1	0	1501000002	1	1	0	1	果然是一表人才，我辈后继有人，这是我罗浮入门秘笈，拿去仔细研习。以后每升5级，你都会获得一个技能点，方可再学习其它技能。	ffffff	打开背包，右键点击技能书，学习技能	completeTask:1501000002;giveTask:1510000009;completeTask:1510000009;giveTask:1501000004										
7	没有装备	0	100	0	-1	0	1501000002	0	0	0	1	把装备穿戴整齐再来找我。	ffffff	打开背包，右键点击装备											
8	开始	0	100	0	-1	0	0	0	0	0	1	三月前蚩尤借天星之力，破封而出，妖魔肆虐人间，我罗浮掌门传檄门下弟子，广传仙法以应浩劫，你可愿入我罗浮一派？。	ffffff	我愿赴汤蹈火，在所不辞	goto:获得装备										
9	获得装备	0	100	0	-1	0	0	0	0	0	0	罗浮道法传承至黄帝座下大将－风后，弓箭之术天下无双，而好的武器装备对我派来说尤为重要，我这里有两件东西正合你用，就此赠送于你以做傍身之用。。	ffffff	按下B打开背包，右键点击装备	giveTask:1510000008;completeTask:1510000008;giveTask:1501000002										
10	杀死怪后送药	0	100	0	-1	1501000005	0	0	0	0	1	试练之地危机四伏，我这里有一些师门秘制药品，你随身携带，危机时刻可以救你一命。	ffffff	完成	goto:试炼之地										
11	openCopyList	100	100	0	-1	0	0	0	0	0	0	副本列表	ffffff												
